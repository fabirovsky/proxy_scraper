Proxy scraper
=============

Simple scraper for spys.one (http://spys.one/proxies/) site

To start spider run ``scrapy crawl proxy``. Proxies will be stored in **proxy.db**