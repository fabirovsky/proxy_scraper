from scrapy import Spider
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst

from ..items import ProxyItem


def decrypt_port(codes, pattern):
    """
    Decrypting port using pattern defined in function decrypt_script_pattern()
    """
    port = ''
    for code in codes:
        port += pattern[code]
    return port


def decrypt_script_pattern(pattern):
    """
    This function is used for decrypt js pattern from spys.one site.
    Pattern contains random variables like h8q7=7296; p6u1v2=0^h8q7; 
    for each number form 0 to 9 
    And to get number 0 is used p6u1v2^h8q7.

    The idea of decrypting this pattern is following
    a = 111
    b = 1 ^ a
    Then b ^ a = 1
    Then we can only define that b ^ a = 1 and substitute it to decrypt 
    port values
    """
    # split and remove last empy item
    pattern = pattern.split(';')[:-1:]

    res = {}
    # remove first 10 vars because we need only vars like a = 1 ^ b
    for p in pattern[10:]:
        part1, part2 = p.split('=')

        # get number from part 2
        number, part2 = part2[0], part2[1:]

        item = part1 + part2
        res[item] = number
    return res


class ProxySpider(Spider):
    """
    Spider for scrappind data from spys.one site
    """

    name = "proxy"

    start_urls = [
        'http://spys.one/proxies/',
        'http://spys.one/proxies/1/',
        'http://spys.one/proxies/2/',
        'http://spys.one/proxies/3/',
    ]

    def parse(self, response):
        script_pattern = 'body > script:nth-child(2)::text'
        script_pattern = response.css(script_pattern).extract()[0]

        pattern = decrypt_script_pattern(script_pattern)

        proxy_css = 'tr > td:nth-child(1) > font.spy14:nth-child(2)'
        rows = response.css(proxy_css)
        for row in rows:
            l = ItemLoader(item=ProxyItem(), selector=row)

            l.default_output_processor = TakeFirst()

            # Get code from js.
            # For example if there is +(n4q7s9^s9l2)+(d4u1w3^q7j0) this returns
            # ['n4q7s9^s9l2', 'd4u1w3^q7j0']
            code = l.get_xpath('.//script/text()',
                               re=r'(?<=\+\().*?(?=\))')
            port = decrypt_port(code, pattern)

            l.add_xpath('ip', './text()')
            l.add_value('port', port)
            yield l.load_item()
